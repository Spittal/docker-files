# Spittal's Laravel Docker

## Heads up! this isn't quite ready for production yet. Sorry.

To use in a new project just copy `.devops`, `docker-compose.yml` and `docker-compose.override.yml` into a new laravel install. You'll need `docker`, a machine running `docker`, and `docker-compose`.

For deps you'll need `composer`, `bower` and `yarn`.

Replace all instances of example in the override file, then...

```bash
bower install --allow-root --force && composer install && yarn && gulp --production

docker-compose up
```

## How to push new base docker files.

Make your edits to the dockerfile of choice, then cd into that directory and run

```bash
docker build -t spittal/laravel-{whatever}:{your tag} .
```

Make sure you have run `docker login` to log into docker hub, be sure you are part of the spittal docker hub organization.

```bash
docker push spittal/laravel-{whatever}:{your tag}
```


## If you don't want to download yarn, bower, or composer on your local machine

Well this is actually the preferred method, but it's slow, and Bower really doesn't like this method. Plus node-sass basically just breaks.

```bash
docker run -v $(pwd):/var/www spittal/composer-node composer install && bower install --allow-root --force && yarn && gulp --production
```

## Bash one-liner to update
Make sure you have run `docker login` to log into docker hub, be sure you are part of the spittal docker hub organization.

find and replace `1.0.0` (including this one) in this command with your desired version.

```bash
docker build -t spittal/laravel-alpine:1.0.0 ./dockerfiles/laravel-alpine && \
docker tag spittal/laravel-alpine:1.0.0 spittal/laravel-alpine:latest && \
docker push spittal/laravel-alpine:1.0.0 && docker push spittal/laravel-alpine:latest && \

docker build -t spittal/laravel-fpm:1.0.0 ./dockerfiles/fpm && \
docker tag spittal/laravel-fpm:1.0.0 spittal/laravel-fpm:latest && \
docker push spittal/laravel-fpm:1.0.0 && docker push spittal/laravel-fpm:latest && \

docker build -t spittal/laravel-fpm-xdebug:1.0.0 ./dockerfiles/fpm-xdebug && \
docker tag spittal/laravel-fpm-xdebug:1.0.0 spittal/laravel-fpm-xdebug:latest && \
docker push spittal/laravel-fpm-xdebug:1.0.0 && docker push spittal/laravel-fpm-xdebug:latest && \

docker build -t spittal/laravel-nginx:1.0.0 ./dockerfiles/nginx && \
docker tag spittal/laravel-nginx:1.0.0 spittal/laravel-nginx:latest && \
docker push spittal/laravel-nginx:1.0.0 && docker push spittal/laravel-nginx:latest && \

docker build -t spittal/laravel-cron:1.0.0 ./dockerfiles/cron && \
docker tag spittal/laravel-cron:1.0.0 spittal/laravel-cron:latest && \
docker push spittal/laravel-cron:1.0.0 && docker push spittal/laravel-cron:latest && \

docker build -t spittal/laravel-worker:1.0.0 ./dockerfiles/worker && \
docker tag spittal/laravel-worker:1.0.0 spittal/laravel-worker:latest && \
docker push spittal/laravel-worker:1.0.0 && docker push spittal/laravel-worker:latest

docker build -t spittal/laravel-socket:1.0.0 ./dockerfiles/socket && \
docker tag spittal/laravel-socket:1.0.0 spittal/laravel-socket:latest && \
docker push spittal/laravel-socket:1.0.0 && docker push spittal/laravel-socket:latest
```

then be sure to add the change the version in the `docker-compose.yml` & `docker-compose.xdebug.yml` files.
