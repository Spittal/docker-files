# Nginx Node

Build from the official NGINX image. With Node and, Yarn installed on top.

## Bash one-liner to update
Make sure you have run `docker login` to log into docker hub, be sure you are part of the SBVR docker hub organization.

find and replace `2.0.1` (including this one) in this command with your desired version.

```bash
docker build -t sbvr/nginx-node:2.0.1 . && \
docker tag sbvr/nginx-node:2.0.1 sbvr/nginx-node:latest && \
docker push sbvr/nginx-node:2.0.1 && docker push sbvr/nginx-node:latest
```
